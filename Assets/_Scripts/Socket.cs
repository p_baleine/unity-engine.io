﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using EngineIO.Client;
using EngineIO.Parser;

namespace EngineIO
{
	namespace Client
	{
		public class Socket : Emitter
		{
			public const string EVENT_OPEN = "open";

			private bool secure;
			private bool agent;
			private string hostname;
			private int port;
			private Dictionary<string, string> query;
			private bool upgrade;
			private string path;
			private bool forceJSONP;
			private bool jsonp;
			private bool forceBase64;
			private bool enablesXDR;
			private string timestampParam;
			private bool timestampRequests;
			private List<string> transports;
			private string readyState;
			//private Link edList<Packet> writeBuffer = new LinkedList<Packet> ();
			// private LinkedList<Delegate> callbackBuffer = LinkedList<Delegate> ();
			private int policyPort;
			private bool rememberUpgrade;
			private bool binaryType;
			private bool onlyBinaryUpgrades;
			private bool perMessageDeflate;

			public Socket () : this (new Options ()) {}

			/// <summary>
			/// Creates a socket.
			/// </summary>
			/// <param name="uri">uri to connect.</param>
			/// <exception cref="UriFormatException"></exception>
			public Socket (string uri) : this (uri, null) {}

			public Socket (Uri uri) : this (uri, null) {}

			/// <summary>
			/// Creates a socket with options.
			/// </summary>
			/// <param name="uri">uri to connect.</param>
			/// <param name="opts">options for socket.</param>
			/// <exception cref="UriFormatException"></exception>
			public Socket (string uri, Options opts) : this (uri == null ? null : new Uri (uri), opts) {}

			public Socket (Uri uri, Options opts) : this (uri == null ? opts : Options.fromUri (uri, opts)) {}

			public Socket (Options opts)
			{
				this.secure = opts.secure;

				if (opts.host != null) {
					string[] pieces = opts.host.Split (':');
					opts.hostname = pieces[0];
					if (pieces.Length > 1) {
						opts.port = int.Parse (pieces[pieces.Length - 1]);
					} else if (opts.port == null) {
						// If no port is specified manually, use the protocol default.
						opts.port = this.secure ? 443 : 80;
					}
				}

				// TODO sslcontext?

				this.hostname = opts.hostname != null ? opts.hostname : "localhost";
				this.port = opts.port != null ? opts.port : (this.secure ? 443 : 80);
				//this.query = opts.query != null ? ParseQS.decode (opts.query) : new Dictionary<string, string> ();
			}

			public Socket Open ()
			{
				return this;
			}

			public class Options : Transport.Options
			{
				/// <summary>
				/// List of transport names.
				/// </summary>
				public string[] transports;

				/// <summary>
				/// Whethre to upgrade the transport. Defaults to `true`.
				/// </summary>
				public bool upgrade = true;

				public bool rememberUpgrade;
				public string host;
				public string query;

				public static Options fromUri (Uri uri, Options opts)
				{
					if (opts == null) {
						opts = new Options ();
					}

					opts.host = uri.Host;
					opts.secure = "https".Equals (uri.Scheme) || "wss".Equals (uri.Scheme);
					opts.port = uri.Port;

					string query = uri.Query;
					if (query != null) {
						opts.query = query;
					}

					return opts;
				}
			}
		}
	}
}
