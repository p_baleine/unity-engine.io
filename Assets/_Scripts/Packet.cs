﻿using UnityEngine;
using System.Collections;

namespace EngineIO
{
	namespace Parser
	{
		public class Packet<T> where T : class
		{
			public const string OPEN = "open";
			public const string CLOSE = "close";
			public const string PING = "ping";
			public const string PONG = "pong";
			public const string UPGRADE = "upgrade";
			public const string MESSAGE = "message";
			public const string NOOP = "noop";
			public const string ERROR = "error";

			public string type;
			public T data;

			public Packet (string type) : this (type, null) {}

			public Packet (string type, T data)
			{
				this.type = type;
				this.data = data;
			}
		}
	}
}
