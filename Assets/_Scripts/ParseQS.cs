﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

namespace ParseQS
{
	public class Querystring
	{
		public static string Encode (Dictionary<string, string> obj)
		{
			StringBuilder str = new StringBuilder ();

			foreach (var pair in obj) {
				if (str.Length > 0)
					str.Append ("&");
				str.Append (Uri.EscapeUriString (pair.Key)).Append ("=")
				            .Append (Uri.EscapeUriString (pair.Value));
			}

			return str.ToString ();
		}

		public static Dictionary<string, string> Decode (string qs)
		{
			Dictionary<string, string> qry = new Dictionary<string, string> ();
			string[] pairs = qs.Split ('&');

			foreach (string pair in pairs) {
				string[] _pair = pair.Split ('=');
				qry[Uri.UnescapeDataString (_pair[0])] = Uri.UnescapeDataString (_pair[1]);
			}

			return qry;
		}
	}
}
