﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EngineIO.Client;

namespace EngineIO
{
	namespace Client
	{
		public class Transport
		{
			public class Options
			{
				public string hostname;
				public string path;
				public string timestampParam;
				public bool secure;
				public bool timestampRequests;
				public int port = -1;
				public int policyPort = -1;
				public Dictionary<string, string> query;
				protected Socket socket;
			}
		}
	}
}
