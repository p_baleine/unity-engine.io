﻿using System.Collections;
using System.Collections.Generic;

public class Emitter
{
	public delegate void EmitterDelegate (params object[] args);
	
	private Dictionary<string, List<EmitterDelegate>> _callbacks;
	
	public Emitter ()
	{
		_callbacks = new Dictionary<string, List<EmitterDelegate>> ();
	}
	
	public Emitter On (string e, EmitterDelegate fn)
	{
		List<EmitterDelegate> callbacks = GetCallbacks (e);
		if (callbacks == null)
			_callbacks["$" + e] = callbacks = new List<EmitterDelegate> ();
		callbacks.Add (fn);
		return this;
	}
	
	public Emitter Once (string e, EmitterDelegate fn)
	{
		EmitterDelegate on;
		on = (args) => {
			Off (e, on);
			fn (args);
		};
		On (e, on);
		return this;
	}
	
	public Emitter Off ()
	{
		_callbacks.Clear ();
		return this;
	}
	
	public Emitter Off (string e)
	{
		_callbacks.Remove ("$" + e);
		return this;
	}
	
	public Emitter Off (string e, EmitterDelegate fn)
	{
		List<EmitterDelegate> callbacks = GetCallbacks (e);
		if (callbacks != null) {
			int idx = callbacks.IndexOf (fn);
			callbacks.RemoveAt (idx);
		}
		return this;
	}
	
	public Emitter Emit (string e, params object[] args)
	{
		List<EmitterDelegate> callbacks = GetCallbacks (e);
		if (callbacks != null) {
			for (int i = callbacks.Count - 1; i >= 0; i--) {
				callbacks[i] (args);
			}
		}
		return this;
	}
	
	public List<EmitterDelegate> Listeners (string e)
	{
		return GetCallbacks (e);
	}
	
	public bool HasListeners (string e)
	{
		List<EmitterDelegate> callbacks = GetCallbacks (e);
		return callbacks != null && callbacks.Count > 0;
	}
	
	private List<EmitterDelegate> GetCallbacks (string e)
	{
		string key = "$" + e;
		List<EmitterDelegate> callbacks = null;
		_callbacks.TryGetValue (key, out callbacks);
		return callbacks;
	}
}