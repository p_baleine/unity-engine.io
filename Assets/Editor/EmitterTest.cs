﻿using UnityEngine;
using System.Collections;
using NUnit.Framework;

[TestFixture]
public class EmitterTest
{
	Emitter e;
	
	[SetUp]
	public void Init ()
	{
		e = new Emitter ();
	}
	
	[Test]
	public void OnTest ()
	{
		bool called = false;
		e.On ("foo", x => called = true);
		e.Emit ("foo");
		Assert.True (called);
		
		int[] args = null;
		e.On ("bar", x => {
			args = System.Array.ConvertAll(x, y => int.Parse (string.Format ("{0}", y)));
		});
		e.Emit ("bar", 1, 2, 3);
		Assert.AreEqual (new int[] {1, 2, 3}, args);
	}
	
	[Test]
	public void OnceTest ()
	{
		int count = 0;
		e.Once ("foo", x => count++);
		e.Emit ("foo");
		e.Emit ("foo");
		Assert.AreEqual (1, count);
	}
	
	[Test]
	public void OffAllTest ()
	{
		bool calledFoo = false;
		bool calledBar = false;
		e.On ("foo", x => calledFoo = true);
		e.On ("bar", x => calledBar = true);
		e.Off ();
		e.Emit ("foo");
		e.Emit ("bar");
		Assert.False (calledFoo);
		Assert.False (calledBar);
	}
	
	[Test]
	public void OffEventTest ()
	{
		bool called = false;
		e.On ("foo", x => called = true);
		e.Off ("foo");
		e.Emit ("foo");
		Assert.False (called);
	}
	
	[Test]
	public void OffCallbackTest ()
	{
		bool calledFoo = false;
		bool calledFoo2 = false;
		Emitter.EmitterDelegate d = x => calledFoo2 = true;
		e.On ("foo", x => calledFoo = true);
		e.On ("foo", d);
		e.Off ("foo", d);
		e.Emit ("foo");
		Assert.True (calledFoo);
		Assert.False (calledFoo2);
	}
	
	[Test]
	public void ListenersTest ()
	{
		e.On ("foo", x => {});
		e.On ("foo", x => {});
		Assert.AreEqual (2, e.Listeners ("foo").Count);
	}
	
	[Test]
	public void HasListenersTest ()
	{
		Assert.False (e.HasListeners ("foo"));
		e.On ("foo", x => {});
		Assert.True (e.HasListeners ("foo"));
	}
	
	[Test]
	public void NotRegisteredEventTest ()
	{
		Assert.DoesNotThrow (() => e.Emit ("foo"));
	}
}