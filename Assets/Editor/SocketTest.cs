﻿using UnityEngine;
using System.Collections;
using System;
using NUnit.Framework;
using EngineIO.Client;

[TestFixture]
public class SocketTest
{
	[SetUp]
	public void Init ()
	{
	}

	[Test]
	public void ConstructorTest ()
	{
		Socket s;
		Assert.DoesNotThrow (() => s = new Socket ());
		Assert.DoesNotThrow (() => s = new Socket (new Socket.Options ()));
		Assert.DoesNotThrow (() => s = new Socket ("ws://localhost"));
		Assert.DoesNotThrow (() => s = new Socket (new Uri ("ws://localhost")));
		Assert.DoesNotThrow (() => s = new Socket ("ws://localhost", new Socket.Options ()));
		Assert.DoesNotThrow (() => s = new Socket (new Uri ("ws://localhost"), new Socket.Options ()));
	}
	
	[Test]
	[Ignore("Pending test")]
	public void OnTest ()
	{
		Socket s = new Socket ("ws://localhost");
		bool called = false;
		s.On (Socket.EVENT_OPEN, x => called = true);
		s.Open ();
		Assert.IsTrue (called);
	}
}