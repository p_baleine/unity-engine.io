﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using ParseQS;

[TestFixture]
public class QuerystringTest
{
	[Test]
	public void EncodeTest ()
	{
		Dictionary<string, string> obj = new Dictionary<string, string> ();
		obj ["foo"] = "1";
		obj ["bar"] = "baz";
		string qs = Querystring.Encode (obj);
		Assert.AreEqual ("foo=1&bar=baz", qs);
	}

	[Test]
	public void DecodeTest ()
	{
		Dictionary<string, string> obj = Querystring.Decode ("foo=1&bar=baz");
		Assert.AreEqual (new Dictionary<string, string> { {"foo", "1"}, {"bar", "baz"} }, obj);
	}
}